module beeper

go 1.13

require (
	github.com/dbatbold/beep v1.0.1
	github.com/faiface/beep v1.0.2
	github.com/hajimehoshi/go-mp3 v0.1.1
	github.com/hajimehoshi/oto v0.3.1
)
