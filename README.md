<h3>Beeper</h3>

## Overview

This project features the use of the `go-mp3` and `oto` libraries in order to play MP3 files using Go.

<br/><h2>Table of Contents</h2>

- [Overview](#overview)
- [How To Run](#how-to-run)

<br/>

## How To Run

Make sure you have go installed and are in the directory with `main.go` in your terminal or command prompt:
```
go run .
```

<br/>

Tinker on :)
