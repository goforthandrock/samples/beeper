package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"time"

	"github.com/hajimehoshi/go-mp3"
	"github.com/hajimehoshi/oto"
)

var player *oto.Player

func main() {
	defer player.Close()

	//start loop where you sleep for 20 seconds, then 10, then repeat
	for {
		time.Sleep(time.Second * 20)

		go beep()

		time.Sleep(time.Second * 10)

		go beep()
	}
}

func beep() {
	fmt.Println("BEEP!")

	f, err := os.Open("beep.mp3")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	d, err := mp3.NewDecoder(f)
	if err != nil {
		log.Fatal(err)
	}

	if player == nil {
		player, err = oto.NewPlayer(d.SampleRate(), 2, 2, 8192)
		if err != nil {
			log.Fatal(err)
		}
	}

	// fmt.Printf("Length: %d[bytes]\n", d.Length())

	if _, err := io.Copy(player, d); err != nil {
		log.Fatal(err)
	}
}
